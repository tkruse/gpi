#!/usr/bin/env groovy
import com.beust.jcommander.JCommander
import com.beust.jcommander.Parameter
import com.beust.jcommander.Parameters
import groovy.grape.Grape
import groovy.text.SimpleTemplateEngine
import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient
import org.apache.ivy.util.DefaultMessageLogger
import org.apache.ivy.util.Message
import org.apache.tools.ant.Project
import org.apache.tools.ant.ProjectHelper
import org.apache.tools.ant.Task
import org.apache.tools.ant.taskdefs.Chmod

import java.util.jar.Attributes
import java.util.jar.Manifest
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.zip.ZipFile

@Grapes([@Grab(group='com.beust', module='jcommander', version='1.47'),
         @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1')])
public class GpiMain {
    public static void main (String[] args) {
        System.setProperty('groovy.grape.report.downloads', 'true')
        CommandMain cm = new CommandMain();
        JCommander jc = new JCommander(cm);

        CommandSearch search = new CommandSearch();
        jc.addCommand("search", search);
        CommandInstall install = new CommandInstall();
        jc.addCommand("install", install);

        jc.parse(args);

        if (!jc.parsedCommand) {
            jc.usage()
            return
        }

        switch(jc.parsedCommand) {
            case 'search':
                search.run()
                break
            case 'install':
                install.run()
                break
            default:
                throw new UnsupportedOperationException(jc.parsedCommand.toString())
        }

    }
}

class MavenCentralClient {

    static String colonPatternToQuery(String pattern, boolean useLatest) {
        if (!pattern.contains(':')) {
            return pattern
        }
        String [] parts = pattern.split(':')
        if (parts.length > 3) {
            throw new IllegalArgumentException("Too many parts in $pattern")
        }
        def validparts = []
        if (parts[0]) {
            validparts.add("g%3A${parts[0]}")
        }
        if (parts.length > 1 && parts[1]) {
            validparts.add("a%3A${parts[1]}")
        }
        if (parts.length > 2 && parts[2]) {
            validparts.add("v:${parts[2]}")
        }
        def query = validparts.join('+AND+')
        // if we give group and artifact, we might be interested on all versions, or just the latest
        if (validparts.size() == 2 && !useLatest) {
            // List all versions
            query += '&core=gav'
        }
        return query
    }

    static List findPackage(String name, boolean useLatest=false) {
        def path = 'http://search.maven.org/solrsearch/select'
        def mavenrepo = new RESTClient(path)
        def queryString = 'q=' + colonPatternToQuery(name, useLatest) + '&rows=1000&wt=json'
        try {

            def resp = mavenrepo.get([queryString: queryString])
            assert resp.status == 200
            if (resp.data.spellcheck && resp.data.spellcheck.suggestions) {
                println "Did you mean ${resp.data.spellcheck.suggestions[1].suggestion}"
            }

            if (resp.data.response.docs.size() > 1) {
                println 'Candidates:'
                resp.data.response.docs.each { println "* $it.id" }
            }
            return resp.data.response.docs
        } catch (HttpResponseException e) {
            throw new IllegalStateException("Request to $path/$queryString failed", e)
        }

    }
}

@Parameters(separators = "=", commandDescription = "Package manager for JVM packages")
class CommandMain {

    @Parameter(names = "--help", help = true)
    private boolean help;
}

@Parameters(separators = "=", commandDescription = "Searches for package by name")
class CommandSearch {

    @Parameter(description = "The name to search for", required = true)
    private List<String> names;

    public run() {
        names.each {
            def candidates = MavenCentralClient.findPackage(it)
            if (candidates.size() == 1) {
                def identifier = candidates[0].id
                candidates = MavenCentralClient.findPackage(identifier)
                if (candidates.size() == 1) {
                    println candidates[0].id
                }
            }
        }

    }

}

@Parameters(separators = "=", commandDescription = "Installs a package")
class CommandInstall {

    @Parameter(description = "The package to install", required = true)
    private List<String> names;

    public run() {
        def installables = []
        // TODO: If name is fully qualified, just install
        names.each {
            def candidates = MavenCentralClient.findPackage(it, true)
            if (candidates.size() == 1) {
                installables.add(candidates[0])
            } else {
                println "Could not install package $it"
            }
        }
        installables.each {
            Map args = [group: it.g, module: it.a, version: it.latestVersion]

            Grape.getInstance()
            Message.setDefaultLogger(new DefaultMessageLogger(Message.MSG_WARN))


            // need to resolve argument as dependencies for some unknown reason... so first arg is [:]
            // resolving actually also downloads jar
            def uris = Grape.resolve([:], args)
            if (!uris) {
                return
            }

            ZipFile jarFileToInstall = new ZipFile(new File(uris[0]));
            InputStream manifestStream = jarFileToInstall.getInputStream(jarFileToInstall.getEntry('META-INF/MANIFEST.MF'))
            Manifest manifest = new Manifest(manifestStream)
            final Attributes.Name MAIN_CLASS_KEY = new Attributes.Name('Main-Class')
            if (manifest.mainAttributes.containsKey(MAIN_CLASS_KEY)) {

                def classpath = []

                for (URI uri : uris) {
                    if (uri.scheme == 'file') {
                        classpath.add(new File(uri))
                    } else {
                        // TODO: Not sure what to do here...
                        // classpath += uri.toASCIIString()
                    }
                }
                // TODO: Check name was not taken yet
                def appName = it.a
                def mainClassName = manifest.mainAttributes.getValue(MAIN_CLASS_KEY)
                println "creating launcher script '$appName' for Main class $mainClassName"

                // hacking into gradle for start scripts, will have to extract those classes / methods
                CreateStartScripts startScriptTask = new CreateStartScripts()

                // by convention, use artifact-name. Not sure what else to use for now
                startScriptTask.applicationName = appName
                startScriptTask.outputDir = new File(Grape.getInstance().getGrapeDir(), 'gpi')
                startScriptTask.mainClassName = mainClassName
                startScriptTask.classpath = classpath
                startScriptTask.generate()
            }




            // works
            // println(ivyInstance.listRevisions('junit', 'junit'))

            //uris.each {println it}
            // check whether manifest declares mainClass

            // if so, create runnableScript

            // symlink into app-folder
        }
    }
}




/***********************************
 *
 * GRADLE RIP-OFF
 *
 ********************************/



/**
 * Shamelessly copied from Gradle, because gradle-plugins jar not available for Grape
 */
public class CreateStartScripts {

    /**
     * The directory to write the scripts into.
     */
    File outputDir

    /**
     * The application's main class.
     */
    String mainClassName

    /**
     * The application's default JVM options.
     */
    Iterable<String> defaultJvmOpts = []

    /**
     * The application's name.
     */
    String applicationName

    String optsEnvironmentVar

    String exitEnvironmentVar

    /**
     * The class path for the application.
     */
    List<File> classpath

    /**
     * Converts an arbitrary string to upper case identifier with words separated by _. Eg, camelCase -> CAMEL_CASE
     */
    public static String toConstant(CharSequence string) {
        if (string == null) {
            return null;
        }
        return toWords(string, '_').toUpperCase();
    }


    private static final Pattern UPPER_LOWER = Pattern.compile("(\\p{Upper}*)(\\p{Lower}*)");

    public static String toWords(CharSequence string, String separator) {
        if (string == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        int pos = 0;
        Matcher matcher = UPPER_LOWER.matcher(string);
        while (pos < string.length()) {
            matcher.find(pos);
            if (matcher.end() == pos) {
                // Not looking at a match
                pos++;
                continue;
            }
            if (builder.length() > 0) {
                builder.append(separator);
            }
            String group1 = matcher.group(1).toLowerCase();
            String group2 = matcher.group(2);
            if (group2.length() == 0) {
                builder.append(group1);
            } else {
                if (group1.length() > 1) {
                    builder.append(group1.substring(0, group1.length() - 1));
                    builder.append(separator);
                    builder.append(group1.substring(group1.length() - 1));
                } else {
                    builder.append(group1);
                }
                builder.append(group2);
            }
            pos = matcher.end();
        }

        return builder.toString();
    }

    /**
     * Returns the name of the application's OPTS environment variable.
     */
    String getOptsEnvironmentVar() {
        if (optsEnvironmentVar) {
            return optsEnvironmentVar
        }
        if (!getApplicationName()) {
            return null
        }
        return "${toConstant(getApplicationName())}_OPTS"
    }

    String getExitEnvironmentVar() {
        if (exitEnvironmentVar) {
            return exitEnvironmentVar
        }
        if (!getApplicationName()) {
            return null
        }
        return "${toConstant(getApplicationName())}_EXIT_CONSOLE"
    }

    File getUnixScript() {
        return new File(getOutputDir(), getApplicationName())
    }

    File getWindowsScript() {
        return new File(getOutputDir(), "${getApplicationName()}.bat")
    }

    void generate() {
        def generator = new StartScriptGenerator()
        generator.applicationName = getApplicationName()
        generator.mainClassName = getMainClassName()
        generator.defaultJvmOpts = getDefaultJvmOpts()
        generator.optsEnvironmentVar = getOptsEnvironmentVar()
        generator.exitEnvironmentVar = getExitEnvironmentVar()
        generator.classpath = getClasspath().collect { File it -> "${it.absolutePath}" }
        generator.scriptRelPath = "bin/${getUnixScript().name}"
        generator.generateUnixScript(getUnixScript())
        generator.generateWindowsScript(getWindowsScript())
    }
}


class StartScriptGenerator {

    final Map<String, String> SCRIPTS = ['unixStartScript.txt' : '''#!/usr/bin/env bash

##############################################################################
##
##  ${applicationName} start up script for UN*X
##
##############################################################################

# Add default JVM options here. You can also use JAVA_OPTS and ${optsEnvironmentVar} to pass JVM options to this script.
DEFAULT_JVM_OPTS=${defaultJvmOpts}

APP_NAME="${applicationName}"
APP_BASE_NAME=`basename "\\$0"`

# Use the maximum available, or set MAX_FD != -1 to use that value.
MAX_FD="maximum"

warn ( ) {
    echo "\\$*"
}

die ( ) {
    echo
    echo "\\$*"
    echo
    exit 1
}

# OS specific support (must be 'true' or 'false').
cygwin=false
msys=false
darwin=false
case "`uname`" in
  CYGWIN* )
    cygwin=true
    ;;
  Darwin* )
    darwin=true
    ;;
  MINGW* )
    msys=true
    ;;
esac

# For Cygwin, ensure paths are in UNIX format before anything is touched.
if \\$cygwin ; then
    [ -n "\\$JAVA_HOME" ] && JAVA_HOME=`cygpath --unix "\\$JAVA_HOME"`
fi

# Attempt to set APP_HOME
# Resolve links: \\$0 may be a link
PRG="\\$0"
# Need this for relative symlinks.
while [ -h "\\$PRG" ] ; do
    ls=`ls -ld "\\$PRG"`
    link=`expr "\\$ls" : '.*-> \\\\(.*\\\\)\\$'`
    if expr "\\$link" : '/.*' > /dev/null; then
        PRG="\\$link"
    else
        PRG=`dirname "\\$PRG"`"/\\$link"
    fi
done
SAVED="`pwd`"
cd "`dirname \\"\\$PRG\\"`/${appHomeRelativePath}" >&-
APP_HOME="`pwd -P`"
cd "\\$SAVED" >&-

CLASSPATH=$classpath

# Determine the Java command to use to start the JVM.
if [ -n "\\$JAVA_HOME" ] ; then
    if [ -x "\\$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="\\$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="\\$JAVA_HOME/bin/java"
    fi
    if [ ! -x "\\$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: \\$JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

# Increase the maximum file descriptors if we can.
if [ "\\$cygwin" = "false" -a "\\$darwin" = "false" ] ; then
    MAX_FD_LIMIT=`ulimit -H -n`
    if [ \\$? -eq 0 ] ; then
        if [ "\\$MAX_FD" = "maximum" -o "\\$MAX_FD" = "max" ] ; then
            MAX_FD="\\$MAX_FD_LIMIT"
        fi
        ulimit -n \\$MAX_FD
        if [ \\$? -ne 0 ] ; then
            warn "Could not set maximum file descriptor limit: \\$MAX_FD"
        fi
    else
        warn "Could not query maximum file descriptor limit: \\$MAX_FD_LIMIT"
    fi
fi

# For Darwin, add options to specify how the application appears in the dock
if \\$darwin; then
    GRADLE_OPTS="\\$GRADLE_OPTS \\\\"-Xdock:name=\\$APP_NAME\\\\" \\\\"-Xdock:icon=\\$APP_HOME/media/gradle.icns\\\\""
fi

# For Cygwin, switch paths to Windows format before running java
if \\$cygwin ; then
    APP_HOME=`cygpath --path --mixed "\\$APP_HOME"`
    CLASSPATH=`cygpath --path --mixed "\\$CLASSPATH"`

    # We build the pattern for arguments to be converted via cygpath
    ROOTDIRSRAW=`find -L / -maxdepth 1 -mindepth 1 -type d 2>/dev/null`
    SEP=""
    for dir in \\$ROOTDIRSRAW ; do
        ROOTDIRS="\\$ROOTDIRS\\$SEP\\$dir"
        SEP="|"
    done
    OURCYGPATTERN="(^(\\$ROOTDIRS))"
    # Add a user-defined pattern to the cygpath arguments
    if [ "\\$GRADLE_CYGPATTERN" != "" ] ; then
        OURCYGPATTERN="\\$OURCYGPATTERN|(\\$GRADLE_CYGPATTERN)"
    fi
    # Now convert the arguments - kludge to limit ourselves to /bin/sh
    i=0
    for arg in "\\$@" ; do
        CHECK=`echo "\\$arg"|egrep -c "\\$OURCYGPATTERN" -`
        CHECK2=`echo "\\$arg"|egrep -c "^-"`                                 ### Determine if an option

        if [ \\$CHECK -ne 0 ] && [ \\$CHECK2 -eq 0 ] ; then                    ### Added a condition
            eval `echo args\\$i`=`cygpath --path --ignore --mixed "\\$arg"`
        else
            eval `echo args\\$i`="\\"\\$arg\\""
        fi
        i=\\$((i+1))
    done
    case \\$i in
        (0) set -- ;;
        (1) set -- "\\$args0" ;;
        (2) set -- "\\$args0" "\\$args1" ;;
        (3) set -- "\\$args0" "\\$args1" "\\$args2" ;;
        (4) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" ;;
        (5) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" "\\$args4" ;;
        (6) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" "\\$args4" "\\$args5" ;;
        (7) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" "\\$args4" "\\$args5" "\\$args6" ;;
        (8) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" "\\$args4" "\\$args5" "\\$args6" "\\$args7" ;;
        (9) set -- "\\$args0" "\\$args1" "\\$args2" "\\$args3" "\\$args4" "\\$args5" "\\$args6" "\\$args7" "\\$args8" ;;
    esac
fi

# Split up the JVM_OPTS And ${optsEnvironmentVar} values into an array, following the shell quoting and substitution rules
function splitJvmOpts() {
    JVM_OPTS=("\\$@")
}
eval splitJvmOpts \\$DEFAULT_JVM_OPTS \\$JAVA_OPTS \\$${optsEnvironmentVar}
<% if ( appNameSystemProperty ) { %>JVM_OPTS[\\${#JVM_OPTS[*]}]="-D${appNameSystemProperty}=\\$APP_BASE_NAME"<% } %>

exec "\\$JAVACMD" "\\${JVM_OPTS[@]}" -classpath "\\$CLASSPATH" ${mainClassName} "\\$@"
''',
                                         'windowsStartScript.txt':'''@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  ${applicationName} startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

@rem Add default JVM options here. You can also use JAVA_OPTS and ${optsEnvironmentVar} to pass JVM options to this script.
set DEFAULT_JVM_OPTS=${defaultJvmOpts}

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.\\

set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%${appHomeRelativePath}

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windowz variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%\\$

:execute
@rem Setup the command line

set CLASSPATH=$classpath

@rem Execute ${applicationName}
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %${optsEnvironmentVar}% <% if ( appNameSystemProperty ) { %>"-D${appNameSystemProperty}=%APP_BASE_NAME%"<% } %> -classpath "%CLASSPATH%" ${mainClassName} %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable ${exitEnvironmentVar} if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%${exitEnvironmentVar}%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega

''']

    /**
     * The display name of the application
     */
    String applicationName

    /**
     * The environment variable to use to provide additional options to the JVM
     */
    String optsEnvironmentVar

    /**
     * The environment variable to use to control exit value (windows only)
     */
    String exitEnvironmentVar

    String mainClassName

    Iterable<String> defaultJvmOpts = []

    /**
     * The classpath, relative to the application home directory.
     */
    Iterable<String> classpath

    /**
     * The path of the script, relative to the application home directory.
     */
    String scriptRelPath

    /**
     * This system property to use to pass the script name to the application. May be null.
     */
    String appNameSystemProperty

    private final engine = new SimpleTemplateEngine()

    /**
     * Returns the line separator for Windows.
     */
    public static String getWindowsLineSeparator() {
        return "\r\n";
    }

    /**
     * Returns the line separator for Unix.
     */
    public static String getUnixLineSeparator() {
        return "\n";
    }

    void generateUnixScript(File unixScript) {
        String nativeOutput = generateUnixScriptContent()
        writeToFile(nativeOutput, unixScript)
        createExecutablePermission(unixScript)
    }

    String generateUnixScriptContent() {
        def unixClassPath = classpath.collect { "${it.replace('\\', '/')}" }.join(":")
        def quotedDefaultJvmOpts = defaultJvmOpts.collect{
            //quote ', ", \, $. Probably not perfect. TODO: identify non-working cases, fail-fast on them
            it = it.replace('\\', '\\\\')
            it = it.replace('"', '\\"')
            it = it.replace(/'/, /'"'"'/)
            it = it.replace(/`/, /'"`"'/)
            it = it.replace('$', '\\$')
            (/"${it}"/)
        }
        //put the whole arguments string in single quotes, unless defaultJvmOpts was empty,
        // in which case we output "" to stay compatible with existing builds that scan the script for it
        def defaultJvmOptsString = (quotedDefaultJvmOpts ? /'${quotedDefaultJvmOpts.join(' ')}'/ : '""')
        def binding = [applicationName: applicationName,
                       optsEnvironmentVar: optsEnvironmentVar,
                       mainClassName: mainClassName,
                       defaultJvmOpts: defaultJvmOptsString,
                       appNameSystemProperty: appNameSystemProperty,
                       appHomeRelativePath: appHomeRelativePath,
                       classpath: unixClassPath]
        return generateNativeOutput('unixStartScript.txt', binding, getUnixLineSeparator())
    }

    void generateWindowsScript(File windowsScript) {
        String nativeOutput = generateWindowsScriptContent()
        writeToFile(nativeOutput, windowsScript);
    }

    String generateWindowsScriptContent() {
        def windowsClassPath = classpath.collect { "%APP_HOME%\\${it.replace('/', '\\')}" }.join(";")
        def appHome = appHomeRelativePath.replace('/', '\\')
        //argument quoting:
        // - " must be encoded as \"
        // - % must be encoded as %%
        // - pathological case: \" must be encoded as \\\", but other than that, \ MUST NOT be quoted
        // - other characters (including ') will not be quoted
        // - use a state machine rather than regexps
        def quotedDefaultJvmOpts = defaultJvmOpts.collect {
            def wasOnBackslash = false
            it = it.collect { ch ->
                def repl = ch
                if (ch == '%') {
                    repl = '%%'
                } else if (ch == '"') {
                    repl = (wasOnBackslash ? '\\' : '') + '\\"'
                }
                wasOnBackslash = (ch == '\\')
                repl
            }
            (/"${it.join()}"/)
        }
        def defaultJvmOptsString = quotedDefaultJvmOpts.join(' ')
        def binding = [applicationName: applicationName,
                       optsEnvironmentVar: optsEnvironmentVar,
                       exitEnvironmentVar: exitEnvironmentVar,
                       mainClassName: mainClassName,
                       defaultJvmOpts: defaultJvmOptsString,
                       appNameSystemProperty: appNameSystemProperty,
                       appHomeRelativePath: appHome,
                       classpath: windowsClassPath]
        return generateNativeOutput('windowsStartScript.txt', binding, getWindowsLineSeparator())

    }

    private void createExecutablePermission(File unixScriptFile) {
        Chmod chmod = new Chmod()
        chmod.file = unixScriptFile
        chmod.perm = "ugo+rx"
        chmod.project = AntUtil.createProject()
        chmod.execute()
    }

    void writeToFile(String scriptContent, File scriptFile) {

        mkdirs(scriptFile.parentFile)
        scriptFile.write(scriptContent)
    }

    /**
     * Converts all line separators in the specified string to the specified line separator.
     */
    public static String convertLineSeparators(String str, String sep) {
        return str == null ? null : str.replaceAll("\r\n|\r|\n", sep);
    }

    private String generateNativeOutput(String templateName, Map binding, String lineSeparator) {
        def templateText = SCRIPTS[templateName]
        def output = engine.createTemplate(templateText).make(binding)
        def nativeOutput = convertLineSeparators(output as String, lineSeparator)
        return nativeOutput;

    }

    private String getAppHomeRelativePath() {
        def depth = scriptRelPath.count("/")
        if (depth == 0) {
            return ""
        }
        return (1..depth).collect {".."}.join("/")
    }

    /**
     * Like {@link java.io.File#mkdirs()}, except throws an informative error if a dir cannot be created.
     *
     * @param dir The dir to create, including any non existent parent dirs.
     */
    public static void mkdirs(File dir) {
        dir = dir.getAbsoluteFile();
        if (dir.isDirectory()) {
            return;
        }

        if (dir.exists() && !dir.isDirectory()) {
            throw new IllegalStateException(String.format("Cannot create directory '%s' as it already exists, but is not a directory", dir));
        }

        List<File> toCreate = new LinkedList<File>();
        File parent = dir.getParentFile();
        while (!parent.exists()) {
            toCreate.add(parent);
            parent = parent.getParentFile();
        }

        Collections.reverse(toCreate);
        for (File parentDirToCreate : toCreate) {
            if (parentDirToCreate.isDirectory()) {
                continue;
            }

            File parentDirToCreateParent = parentDirToCreate.getParentFile();
            if (!parentDirToCreateParent.isDirectory()) {
                throw new IllegalStateException(String.format("Cannot create parent directory '%s' when creating directory '%s' as '%s' is not a directory", parentDirToCreate, dir, parentDirToCreateParent));
            }

            if (!parentDirToCreate.mkdir() && !parentDirToCreate.isDirectory()) {
                throw new IllegalStateException(String.format("Failed to create parent directory '%s' when creating directory '%s'", parentDirToCreate, dir));
            }
        }

        if (!dir.mkdir() && !dir.isDirectory()) {
            throw new IllegalStateException(String.format("Failed to create directory '%s'", dir));
        }
    }
}


public class AntUtil {
    /**
     * @return Factory method to create new Project instances
     */
    public static Project createProject() {
        final Project project = new Project();

        final ProjectHelper helper = ProjectHelper.getProjectHelper();
        project.addReference(ProjectHelper.PROJECTHELPER_REFERENCE, helper);
        helper.getImportStack().addElement("AntBuilder"); // import checks that stack is not empty

        project.init();
        project.getBaseDir();
        return project;
    }

    public static void execute(Task task) {
        task.setProject(createProject());
        task.execute();
    }
}