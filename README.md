
# gpi - Groovy package installer

---
*NOT CONTINUED: See gvm / SDKMAN! instead.*
---


Pre-alpha project to become something like [cpan] / [pip] / [gem] / [npm] for JVM languages.
That is an OS independent packaging system for Free Java Software.
Inspired also by [gvm][gvm] and [jpm4j].

In the current state the gpi script downloads jars by wrapping the standard
Groovy ```grape install``` command, to the Groovy home folder (```.groovy/grape```).
Additionally it creates launcher scripts for executable jars in ```.groovy/gpi```.


## Table of Contents

* [Usage](#usage)
* [Motivation](#motivation)
* [FAQ](#faq)
* [Troubleshoot](#troubleshoot)
* [References](#references)

## Usage

### gpi install

Same syntax as gpi search possible. When unique, grabs latest version.

```
gpi install org.mockito:mockito-all
```

If the specified package is an executable jar, gpi will additionally create
 a launcher script in ```~/.groovy/cpi```

```
gpi install :jftp
```


### gpi search

gpi search command based on [maven central][central] REST api:

```
gpi search mockito
Candidates:
* org.mockito:mockito-all
* org.mockito:mockito-core

gpi search org.mockito:mockito-all
Candidates:
* org.mockito:mockito-all:2.0.2-beta
* org.mockito:mockito-all:2.0.1-beta

gpi search :junit
Candidates:
* junit:junit
* org.technbolts:junit

```


## Motivation


A quick installer targets certain specific application use-cases, it is not generally the best solution for distributing software.

The type of application to be supported are mainly:
- Command line utilities (like a json colorizer)
- Small specialized UI apps (like email clients, Notepad editors)
- Microservices (like tiny http server applications)

The installer is generally probably less useful for:
- complex applications (Office suites, IDEs)
- java libraries
- plugins

The benefit of an installer is:
- No overhead infrastructure, creating an executable jar is sufficient to distribute on many plattforms
- Quick installation (also short commands useful for installation scripts, like on CI servers)

Alternative way to distribute JVM software are:
- provide installers for multiple operating systems
- provide packages via the packaging systems of multiple operating systems
- provide detailed instructions for what to do after downloading a jar/zip/tar or checkout from source control
- provide build-tools plugins, for things like code coverage checkers

### Goals

- install jars locally to a common local repository like ```~/.gradle``` or ```~/.m2```
- install from public repository like [maven central][central] or [bintray]
- install jars using just the module id where possible
- for executable jars, create launcher scripts
- search for packages
- clean removal
- select version to install
- show installed versions (maybe only those given in commands?)
- install runnable scripts based on metadata included in the jars
- when starting the application, the ```current Dir``` is set to some location that is visible to the user
- configure which JRE version to use for each runnable
- allow tool configuration via configuration file

### Draft

* The tool can be installed to the userspace similar to how [gvm] is being installed
* The search command will use a search REST API from a public repository.
* When module to be installed has been identified, the jars will be downloaded as already done by [gradle] or [maven].
* The tool will use [aether] or [ivy] as repo library, possibly wrapping [grape], [gradle], [maven] or similar.
* The tool will scan the main jar for metadata that describes what scripts (runnables) are to be provided and how.
* The tool will check that all scripts to be created do not conflict with existing scripts
* The tool will create a launcher script for each runnable the jar wants to provide, in a path that can be added to the ```PATH```
* metadata will be a json file or whatever is used for [```MANIFEST.MF```][manifest]


* Possibly the [gradle] application plugin or something similar in [maven] can be used to create a local installation

#### Buildsystem-based alternative

A buildsystem provide (like [gradle]) could support this operation
such that by installing the buildsystem, one also gets the command to
install executable jars via the command line.

Another alternative is to create this tool as if it were a "project"
without local sources, with specialized plugins that do not compile
source files, but merely downlload dependencies to the usual location
(.m2, .gradle) and create runnable scripts inside the "project" folder.

Draft for [gradle]:
```
UserHome
- .gradle
-- cache          # existing location of downloaded jars
------- foo.jar   # jar with the classes of example 'installed' app
- .gpi
-- bin
--- gpi           # command to add/introspect/update/remove apps 'installed' in bin
-- apps
--- default       # namespace allowing other profiles (maybe YAGNI)
---- bin          # all runnables
----- foo         # example launch script
----- foo.bat
-- gradlew        # standard gradle wrapper
-- build.gradle   # defines plugins that create launcher script in bin
-- profiles.json  # defines what apps are 'instaled' for what profile
```

The gpi command could then modify the profiles.json, then run a refresh task using gradle.
By putting the ```bin``` and ```default``` folders on the PATH, the runnables become executable from any context in the shell.
This approach also allows users to customize the 'installation' process, additional hooks to include tasks or task definition files could be provided.

The same principle should be viable as well for [maven] or [sbt].


#### Metadata Content

The following information is lacking in the existing metadata:

High Priority
- Name to be used to launch an executable jar (Possible conventions could be the POM project/artifactId, or the (optional) POM Project/Name.
- Further executables bundled in the same jar
- Whether a runtime folder needs to be provided (to store application data, like logs, local configuration)

Lower Priority:
- Further support resources to be deployed (manpages, bash completion)
- short project description to be shown in a list (80 chars or so)
- detailed description to be shown in a detailed view (POM project/description)

Least Priority:
- an Icon to use to create visual shortcuts (Windows start menu/desktop, Ubuntu launcher)
- a pretty name to use to display the program in menus

#### Metadata Storage

Places already exist to place metadata,
- the [```META-INF/MANIFEST.MF```][manifest] file
- the [maven pom][pom] that is released to repositories

The pom may contains things like:

- artefact Ids
- dependencies
- data to build the source (with maven)
- license, authors, affiliated project websites

The POM cannot be extended other than by creating a new version of the
schema definition. The POM is used by maven to perform a build from
source (```pom.xml```). However, the POM is also used to describe a
packaged artifact in a pom repository, regardless of which build
system was used to build (So gradle and sbt can also create such a
reduced POM file to publish an artifact in a maven repository.

The JAR Manifest considers entries relevant to
- standalone jar application
- applets (you remember those?)
- signing and sealing

The JAR Manifest format is generally a key-value format, where the key is something like (```[a-zA-Z0-9-_]+```), and the value any text up to the next line that does not start with a space. Several keys have a fixed meaning, but it seems possible to add more keys.
As values, it is common to have comma-separated lists for structured data. It is conceivable to also stor JSON documents in the value space, provided serialization and deserialization with a leading space is used.

So the following alternatives seem plausible:

1. Define a syntax for metadata inside a ```MANIFEST.MF``` file section to describe extended deployment information
2. Define a syntax and filename (e.g. ```META-INF/DEPLOY.json```) for files describing extended deployment information
3. Extend the POM schema to provide additional metadata

The advantage of extending the POM schema would be that hosts could easily index the additional meta-information.

#### Local Repositories

It is advantageous to download necessary jars only once. Still, several locations already exist that are widely used.

programs | System property | default *nix location | default Windows location | API
---------|-----------------|-----------------------|--------------------------|----
maven, sbt | M2_HOME | ~/.m2 | ? | aether
gradle | GRADLE_USER_HOME | ~/.gradle | C:\Users\UserName\.gradle | ivy
grape | grape.root | ~/.groovy/grape | ? | ivy

#### Application hosting

The initial way to release Java Applications has been to host the jar files privately on FTP/Http servers for manual download and installation. Several Toolkits provided means of creating self-extracting archives or installers for Windows.

[Maven Central][central] has been the first major successful hosting site for jar files. Many build tools are configured by default to fetch jars from that location. It is however notoriously cumbersome to release to maven central, due to several complex interaction steps that are required.

The first alternative to Maven Central were privately run Maven Repositories based on free Software like Nexus or Artifactory. Those however need to be manually configured, and it is difficult to make search queries over many such repositories.

[Bintray jcenter][bintray] is competing with Maven central and offers an improved workflow for releasing software.

At the time of this writing Maven Central offers a public [REST API][central-rest] to query it's search index. Bintray requires an [API key][bintray-rest] with an access limit to use the REST API.

The two major Maven repositories are primarily being used to release library jars, not executable jars.

The [jpm4j] project hosts several applications already.

Going forward to host jar-based applications, it is possible to either attempt to provide an application relying on the existing public maven repositories, or [jpm4j] or a custom hosting/registration solution.

The main advantages of custom hosting are:
- it is possible to list all jars that are actually executable
- it is possible to deliver jars hosted at arbitrary sites (e.g. github)


#### System install

On *nix systems, several Java applications are also available via the respective package manager. Provided common (shareable) jars are placed in:

Distribution | Locations
-------------|----------
Ubuntu | ```/usr/share/java/<artifact>.jar, /usr/share/java/<artifact>-<version>.jar, /usr/share/maven-repo/<organisation>/<version>/<artifact>-<version>.jar```


### Other ideas

* Compatibility with [virtualenv] would be great:
** creation of launch scripts in subfolder as namespace (e.g. .groovy/gpi/default/foo, ./groovy/gpi/java6/foo)
* [FHS][fhs] compliance is to be considered
* Possible Installation of bash completion and [manpages][man] would be great
* plugins / instructions for [maven], [gradle] to create a compatible metadata / distribution folder would be great, once the tool support more than [```MANIFEST.MF```][manifest]
* global installation (in system-space, for all users, [FHS][fhs] compliant)
* project-local installation (similar to node_modules folder with [npm])
* Android App launcher
* support plugins that install via being on the classpath of another app
* Whitelist apps from Maven Central, to not provide false impression
* Support 'installing' jars from non-Maven-repo hosts, as long as a pom (or similar spec) is provided
* Support an (online) registry/index of packages from non-Maven-repo hosts. Registry may provide POMs by 3rd parties (wiki-like)
* PoC 'installation' of Groovy, Eclipse, Tomcat

## FAQ

### Why [groovy]?

As aninstaller this tool should have as few non-(*nix)-standard dependencies as possible. So a set of bash scripts would do nicely.
However it is an installer for JVM apps, so the presence of a JRE is not too high a burden as a dependency.

Then the idea is to leverage existing infrastructure as much as possible, so depending on Apache [ivy] (or [aether]) is highly useful. Also it is valuable to have scripts rather than binaries, because *nix are rightly wary of installing stuff that might contain malware. Scripts are transparent and therefore more trustworthy, in a certain way (In particular while this project has no strong community).

So to get this project started, I chose to create the tool as Groovy Script for prototyping (it offers transparency and a nice wrapper for [ivy]).
After the prototyping / exploration phase, this should be replaced by a jar written in Java bundling only the classes strictly necessary, to be minimal in size and runtime dependencies.

### [ivy] vs. [aether]

Ivy is the underlying system or [ant] and [grape] ([gradle] uses Grape).
Aether is the underlying system of [maven] and [sbt].

Both are very complex to use directly, and a pain to set up for an independent project to reflect the user configuration.

### Difference to [gvm]

* generic tool installing anything from any maven repo as long as the jar contains metadata

### Difference to [jpm4j]

* not linked link to bnd and OSGI, no OSGI features (start and stop services)
* no installations to system space (/usr, /var)
* no custom package index site (for now)

## Troubleshoot

* Grape reports ```[NOT FOUND]``` in localm2:

```
:::: WARNINGS
                [NOT FOUND  ] org.slf4j#slf4j-api;1.6.1!slf4j-api.jar (2ms)

        ==== localm2: trie
```

This happens I think when in the local maven repository (```~/.m2```) the pom is present, but not the jar. Not sure where that state comes from, it is probably valid, but annoying.

For now, the quickest workaround is to tell maven to install the missing plugin, one way is like this:

```
mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:get -DrepoUrl=http://download.java.net/maven/2/ -Dartifact=org.slf4j:slf4j-api:1.6.1
```

Replace the artifact id at then end with whatever is missing for you, split the three parts with colons ```:```.


In the long run this should get fixed in Grape, I think.


## References

### Discussions

* Original gip thread: http://groovy.329449.n5.nabble.com/JVM-application-installer-via-maven-gradle-repo-td5722094.html
* Blog entry for jpm4j: http://softwaresimplexity.blogspot.de/2012/04/just-another-package-manager.html

### Some examples that worked for me

```
./gpi install :crash.shell
./gpi install emma:emma
./gpi install net.sf.jtidy:jtidy
./gpi install :jslint4java
./gpi install java2html:j2h
./gpi install :jftp
./gpi install :tika-app
./gpi install :DataCleaner-desktop
./gpi install org.jboss.aesh:aesh
./gpi install :MutabilityDetector
./gpi install :testability-explorer
./gpi install org.jvnet.ws.wadl:wadl-cmdline
./gpi install batik:batik-ttf2svg
./gpi install :formlayoutmakerx
./gpi install net.sourceforge.jmol:jmol
./gpi install de.jflex:jflex
./gpi install snowizard-application
./gpi install :yaoqiang-bpmn-editor
./gpi install  org.w3c.css:css-validator
```

(more contributions to this list welcome!)

### Links


* [gvm][gvm]
* [grape][grape]
* [groovy][groovy]
* [pip][pip]
* [gem][gem]
* [npm][npm]
* [cpan][cpan]
* [jpm4j][jpm4j]
* [ivy][ivy]
* [aether][aether]
* [maven][maven]
* [gradle][gradle]
* [sbt][sbt]
* [ant][ant]
* [fhs][fhs]
* [central][central]
* [bintray][bintray]
* [virtualenv][virtualenv]
* [man][man]


[gvm]: http://gvmtool.net/
[grape]: http://groovy.codehaus.org/Grape
[groovy]: http://groovy.codehaus.org/
[pip]: https://pip.pypa.io/en/latest/
[gem]: https://rubygems.org/
[npm]: https://www.npmjs.com/
[cpan]: http://www.cpan.org/
[jpm4j]: https://www.jpm4j.org/
[ivy]: http://ant.apache.org/ivy/
[aether]: http://eclipse.org/aether/
[maven]: http://maven.apache.org/
[gradle]: https://www.gradle.org/
[sbt]: http://www.scala-sbt.org/
[ant]: http://ant.apache.org/
[fhs]: http://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
[central]: http://search.maven.org/
[central-rest]: http://search.maven.org/#api
[bintray]: https://bintray.com/
[bintray-rest]: https://bintray.com/docs/api/
[virtualenv]: https://virtualenv.pypa.io/en/latest/
[man]: http://en.wikipedia.org/wiki/Man_page
[manifest]: http://docs.oracle.com/javase/tutorial/deployment/jar/manifestindex.html
[pom]: http://maven.apache.org/pom.html
